#### **-- Users management**

    - log in (done)
    - log out (done)
    - sign up ?make email confirmation required?
    - change password ?make good looking template?
    - reset password ?make good looking template?
    - create user's profile page
    
#### **-- Main features**

    - create and migrate data models
        - Book
        - Genre
        - Review
        - Author
        - BookSeries
        - BookImage
    - create main page with list of newest available books
    - create page with list of authors
    - create page with list of books by author
    - create Book detail page
    - create filtering and serch
    - create reviews app
    - create cart app
    
#### **-- Finalize ToDo**