from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.views.decorators.cache import never_cache
from django.views.static import serve

urlpatterns = [
    # admin stuff
    path('admin/', admin.site.urls),

    # users management
    path('accounts/', include('allauth.urls')),

    # local apps
    path('books/', include('apps.books.urls', namespace='books')),
    path('', include('apps.pages.urls', namespace='')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns.append(path('static/<path:path>', never_cache(serve)))
