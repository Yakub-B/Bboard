from django.utils.text import slugify


RATE_CHOICES = [
    (1, '1 star'), (2, '2 stars'),
    (3, '3 star'), (4, '4 stars'),
    (5, '5 star'), (6, '6 stars'),
    (7, '7 star'), (8, '8 stars'),
    (9, '9 star'), (10, '10 stars'),
]


def slugify_by_title_and_pk(title, pk):
    return slugify(title) + str(pk)
