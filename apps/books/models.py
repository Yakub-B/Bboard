from django.contrib.auth import get_user_model
from django.db import models

from decimal import Decimal

from django.db.models import Avg

from .utils import RATE_CHOICES
from .validators import validate_discount

User = get_user_model()


class Book(models.Model):
    """
    Model for book objects
    """
    title = models.CharField(max_length=255)
    slug = models.SlugField(unique=True, db_index=True)
    description = models.TextField(max_length=1255)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    available = models.BooleanField(default=True)
    pub_year = models.PositiveSmallIntegerField(
        verbose_name='Publication year'
    )
    discount = models.PositiveSmallIntegerField(
        null=True, blank=True, verbose_name=r'% discount',
        validators=[validate_discount]
    )
    edition_number = models.PositiveSmallIntegerField(
        verbose_name='Edition number'
    )
    added = models.DateField(auto_now_add=True)

    # relations
    genre = models.ForeignKey(
        'Genre', on_delete=models.CASCADE, related_name='books'
    )
    authors = models.ManyToManyField('Author', related_name='books')
    series = models.ForeignKey(
        'BookSeries', on_delete=models.CASCADE,
        related_name='books', null=True, blank=True
    )

    def __str__(self):
        return self.title

    def get_rating(self):
        """
        this method returns rating by calculation average value of all
        related reviews rate
        """
        return self.reviews.aggregate(Avg('rate'))

    def get_price_with_discount(self):
        """
        this method calculates price of the book, taking into account
        the discount if there is no discount - returns original price
        """
        if self.discount:
            final_price = Decimal(self.price) * \
                    (1 - Decimal(self.discount) / 100)
            return str(final_price)
        else:
            return str(self.price)


class Genre(models.Model):
    """
    model for book genres
    """
    title = models.CharField(max_length=255)
    slug = models.SlugField(unique=True, db_index=True)

    def __str__(self):
        return self.title

    def get_related_books_num(self):
        """this method returns number of books related to this genre"""
        return len(self.books.all())


class Author(models.Model):
    """
    Model for book authors
    """
    first_name = models.CharField(max_length=255, verbose_name='First name')
    surname = models.CharField(max_length=255, verbose_name='Surname')
    patronymic = models.CharField(max_length=255, verbose_name='Patronymic')
    slug = models.SlugField(unique=True)

    def __str__(self):
        return f'Author: {self.first_name} {self.surname}'

    def get_related_books_num(self):
        """
        this method returns number of books related to this author
        """
        return len(self.books.all())


class BookReview(models.Model):
    """
    Model for book reviews. It is essentially a link table between the book and
    the user models, implementing a many-to-many relationship between them
    """
    book = models.ForeignKey(
        Book, on_delete=models.CASCADE, related_name='reviews'
    )
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='book_reviews'
    )

    rate = models.PositiveSmallIntegerField(choices=RATE_CHOICES)
    body = models.TextField(max_length=1255, blank=True, null=True)

    class Meta:
        # setting that one user can make only one review for each book
        unique_together = ['book', 'user']

    def __str__(self):
        return f'Review for book {self.book.slug} by user {self.user.username}'


class BookSeries(models.Model):
    """
    Model for a series of books (like Harry Potter, etc)
    """
    title = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255)

    def __str__(self):
        return self.title


class BookImage(models.Model):
    """
    Model for book images
    """
    book = models.ForeignKey(
        Book, on_delete=models.CASCADE, related_name='image_gallery'
    )
    image = models.ImageField(upload_to='%Y/%m/%d/')
