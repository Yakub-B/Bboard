from django.contrib import admin
from django.contrib.admin import ModelAdmin

from . import models


class ImageInLine(admin.TabularInline):
    model = models.BookImage


@admin.register(models.Book)
class BookAdmin(ModelAdmin):
    inlines = [
        ImageInLine,
    ]
    list_display = ('title', 'price', 'discount', 'available')
    list_editable = ('discount', 'available')
    prepopulated_fields = {'slug': ('title', 'pub_year')}

    ordering = ['added', ]
    search_fields = ['title', 'pub_year', 'genre__title']

    save_on_top = True


@admin.register(models.Genre)
class GenreAdmin(ModelAdmin):
    list_display = ('title', 'slug')
    prepopulated_fields = {'slug': ('title',)}
    search_fields = ['title']


@admin.register(models.Author)
class AuthorAdmin(ModelAdmin):
    prepopulated_fields = {'slug': ('first_name', 'surname')}


@admin.register(models.BookReview)
class ReviewAdmin(ModelAdmin):
    list_display = ('user', 'book', 'rate')
    readonly_fields = ('user', 'book', 'rate')


admin.site.register(models.BookImage)


@admin.register(models.BookSeries)
class SeriesAdmin(ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
