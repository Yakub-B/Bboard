from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError

from django.db import IntegrityError
from django.test import TestCase

from .validators import validate_discount

from .models import (
    Book, Genre, BookReview,
    Author, BookSeries,
)


def author_genre_book_setup():
    genre = Genre.objects.create(title='Classic', slug='classic')
    author = Author.objects.create(
        first_name='Taras', surname='Shevchenko',
        patronymic='Hryhorovych', slug='taras1',
    )
    book = Book.objects.create(
        id=1, title='Test', slug='test', description='testtest test',
        price=500, available=True, genre=genre,
        pub_year=1840, edition_number=1
    )
    book.authors.add(author)
    return genre, author, book


class ValidateDiscountTest(TestCase):

    def test_validate_discount(self):
        # testing with discount < 0
        with self.assertRaises(ValidationError):
            validate_discount(0)
        # testing with discount > 90
        with self.assertRaises(ValidationError):
            validate_discount(91)
        # testing with right discount value
        self.assertEqual(None, validate_discount(55))


class GenreTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.genre, cls.author, cls.book = author_genre_book_setup()

    def test_genre_creation(self):
        self.assertEqual('Classic', self.genre.title)
        self.assertEqual('classic', self.genre.slug)
        self.assertEqual(1, self.genre.books.first().pk)

    def test_get_related_books_num_method(self):
        related_books_num = self.genre.get_related_books_num()
        self.assertEqual(1, related_books_num)
        self.genre.books.all().delete()
        related_books_num = self.genre.get_related_books_num()
        self.assertEqual(0, related_books_num)


class AuthorTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.genre, cls.author, cls.book = author_genre_book_setup()

    def test_author_creation(self):
        self.assertEqual('Taras', self.author.first_name)
        self.assertEqual('Shevchenko', self.author.surname)
        self.assertEqual('Hryhorovych', self.author.patronymic)
        self.assertEqual('taras1', self.author.slug)
        self.assertEqual(1, self.author.books.first().pk)

    def test_get_related_books_num_method(self):
        related_books_num = self.author.get_related_books_num()
        self.assertEqual(1, related_books_num)
        self.author.books.all().delete()
        related_books_num = self.author.get_related_books_num()
        self.assertEqual(0, related_books_num)


class BookReviewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        user_model = get_user_model()
        cls.user_instance = user_model.objects.create_user(
            username='Test',
            email='test@test.com',
            password='testpassword'
        )
        cls.genre, cls.author, cls.book = author_genre_book_setup()
        cls.review = BookReview.objects.create(
            user=cls.user_instance, book=cls.book,
            rate=10, body='Test review1'
        )

    def test_review_creation(self):
        self.assertEqual('Test review1', self.review.body)
        self.assertEqual(10, self.review.rate)
        self.assertEqual(1, self.review.book_id)
        self.assertEqual(1, self.review.user_id)

    def test_unique_user_book_constraint(self):
        with self.assertRaises(IntegrityError):
            self.review2 = BookReview.objects.create(
                user=self.user_instance, book=self.book,
                rate=5, body='Test review2'
            )


class BookTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.genre, cls.author, cls.book = author_genre_book_setup()
        user_model = get_user_model()
        cls.user_instance = user_model.objects.create_user(
            username='Test',
            email='test@test.com',
            password='testpassword'
        )
        cls.user_instance1 = user_model.objects.create_user(
            username='Test2',
            email='test2@test.com',
            password='testpassword2'
        )
        cls.review = BookReview.objects.create(
            user=cls.user_instance, book=cls.book,
            rate=10, body='Test review1'
        )
        cls.review2 = BookReview.objects.create(
            user=cls.user_instance1, book=cls.book,
            rate=3, body='Test review2'
        )
        cls.series = BookSeries.objects.create(
            title='test_series', slug='test-series'
        )
        cls.book.series = cls.series
        cls.book.save()

    def test_book_creation(self):
        self.assertEqual('Test', self.book.title)
        self.assertEqual('test', self.book.slug)
        self.assertEqual('Classic', self.book.genre.title)
        self.assertEqual('Taras', self.book.authors.first().first_name)
        self.assertEqual('testtest test', self.book.description)
        self.assertEqual(500, self.book.price)
        self.assertEqual(1840, self.book.pub_year)
        self.assertEqual(1, self.book.edition_number)
        self.assertEqual(True, self.book.available)
        self.assertEqual('test_series', self.book.series.title)

    def test_get_price_with_discount_method(self):

        # testing with no discount
        final_price = self.book.get_price_with_discount()
        self.assertEqual('500', final_price)

        # setting discount
        self.book.discount = 15
        self.book.save()
        final_price = self.book.get_price_with_discount()
        self.assertEqual('425.00', final_price)

    def test_get_rating_method(self):

        rating = self.book.get_rating()
        self.assertEqual(6.5, rating['rate__avg'])

        self.book.reviews.all().delete()
        rating = self.book.get_rating()
        self.assertEqual(None, rating['rate__avg'])
