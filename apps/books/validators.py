from django.core.exceptions import ValidationError


def validate_discount(value):
    """
    This validator checks if discount is in range from 1 to 90
    """
    if value > 90 or value < 1:
        raise ValidationError(
            'The discount can only be in the range from 1 to 90%'
        )
