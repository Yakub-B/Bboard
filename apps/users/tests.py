from django.test import TestCase

from django.contrib.auth import get_user_model
from django.urls import reverse


class CustomUserTests(TestCase):

    def test_user_creation(self):
        User = get_user_model()
        user = User.objects.create_user(
            username='Test',
            email='test@test.com',
            password='testpassword'
        )

        self.assertEqual(user.username, 'Test')
        self.assertEqual(user.email, 'test@test.com')
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)

    def test_create_superuser(self):

        User = get_user_model()
        admin_user = User.objects.create_superuser(
            username='superadmin',
            email='superadmin@email.com',
            password='testpass123'
        )

        self.assertEqual(admin_user.username, 'superadmin')
        self.assertEqual(admin_user.email, 'superadmin@email.com')
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)


class SignupPageTest(TestCase):

    username = 'tester'
    email = 'email@test.com'

    def setUp(self):
        self.response = self.client.get(reverse('account_signup'))

    def test_signup_template(self):
        # checking if the right template is used
        self.assertTemplateUsed(self.response, 'account/signup.html')
        # checking status code of the response
        self.assertEqual(self.response.status_code, 200)
        # checking if the right view is used

    def test_signup_form(self):
        get_user_model().objects.create_user(
            self.username, self.email)

        self.assertEqual(get_user_model().objects.all().count(), 1)
        self.assertEqual(
            get_user_model().objects.all()[0].username, self.username
        )
        self.assertEqual(
            get_user_model().objects.all()[0].email, self.email
        )
